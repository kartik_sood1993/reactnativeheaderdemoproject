import React from 'react';
import {View} from 'react-native';
import {Header} from 'react-native-elements';

const App = () => {
  return (
    <View>
      <Header
        backgroundColor="green"
        leftComponent={{
          icon: 'menu',
          color: '#fff',
          iconStyle: {color: '#fff'},
        }}
        centerComponent={{text: 'MY TITLE', style: {color: '#FFFFFF'}}}
        rightComponent={{icon: 'home', color: '#fff'}}
      />
    </View>
  );
};
export default App;
